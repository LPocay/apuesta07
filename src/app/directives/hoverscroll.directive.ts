import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appHoverscroll]'
})
export class HoverscrollDirective {

  constructor(private el: ElementRef) { 
    el.nativeElement.style.overflowY = 'hidden';
  }

  @HostListener('mouseenter')  onMouseEnter () {
    this.el.nativeElement.style.overflowY = 'auto';
  }

  @HostListener('mouseleave')  onMouseLeave () {
    this.el.nativeElement.style.overflowY = 'hidden';
  }

}
