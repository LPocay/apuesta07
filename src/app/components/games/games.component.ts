import { Component, OnInit } from '@angular/core';
import { League, SportEvent, Group } from '../../models/sports';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { elementListAnimation } from '../../animations/list-element.animation';
import { ChangeActiveGroup, ChangeActiveSportEvent } from '../../actions/schedule.actions';
import { GetScore, RemoveScore } from '../../actions/score.actions';
import { GetLineByEvent, IsFetching } from '../../actions/line.actions';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
  animations: [elementListAnimation]
})
export class GamesComponent implements OnInit {
  
  // State of the App
  @Select(state => state.schedule.activeLeague) activeLeague$: Observable<League>;
  @Select(state => state.schedule.activeGroup) activeGroup$: Observable<Group>;
  @Select(state => state.schedule.activeSportEvent) activeSportEvent$: Observable<SportEvent>;

  // Local variables
  league: League;
  groupsFiltered: Group[];
  sportEvents: SportEvent[] = [];  
  searchGroupInput: FormControl;

  activeGroup: Group;
  activeSportEvent: SportEvent;

  constructor(private _store: Store) { }

  ngOnInit() {
    // Se lleva acabo el cambio de estado de la liga activa, el grupo y los eventos
    // de manera que estos puedan ser usado para apuestas rapidas.
    this.activeLeague$.subscribe(
      league => {
        if (league) {
          this.league = league;
          this.groupsFiltered = this.league.group;
          this._store.dispatch(new ChangeActiveGroup(this.league.group[0]));
        }
      }
    );
    this.activeGroup$.subscribe(
      group => {
        if (group){
          this.activeGroup = group;
          if (group.event instanceof Array) {            
            this.sportEvents = group.event;
          } else {
            this.sportEvents = [group.event];
          }
          this._store.dispatch(new ChangeActiveSportEvent(this.sportEvents[0]));
        }        
      }
    );
    this.activeSportEvent$.subscribe(
      sportEvent => {
        if (sportEvent) {
          this.activeSportEvent = sportEvent;
          this._store.dispatch([new IsFetching, new GetLineByEvent(this.league, sportEvent)]);
          if (sportEvent.event_state != 'PENDING') {
            this._store.dispatch(new GetScore(sportEvent));
          } else {
            this._store.dispatch(new RemoveScore);
          }
        } 
      }
    );

    // Inicializacion del input para buscar grupos
    this.searchGroupInput = new FormControl();
    this.onSearchInputChange();
  }

  changeGroup(group: Group): void {
    // Cada vez que el grupo cambie se debe notificar al state
    if (this.activeGroup != group) {
      this._store.dispatch(new ChangeActiveGroup(group));
    }
  }

  onSearchInputChange (): void {
    this.searchGroupInput.valueChanges.pipe(debounceTime(500)).subscribe(
      value => {
        this.groupsFiltered = this.league.group.filter(group => {
          if (!value) {
            return true
          }
          if (group.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())) {
            return true;
          } else {
            return false;
          }
        });
      }
    );
  }
}
