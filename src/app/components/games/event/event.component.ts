import { Component, OnInit, Input } from '@angular/core';
import { SportEvent } from '../../../models/sports';
import { Store } from '@ngxs/store';
import { ChangeActiveSportEvent } from '../../../actions/schedule.actions';
import { GetScore, RemoveScore } from '../../../actions/score.actions';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  @Input('event') sportEvent: SportEvent;
  @Input('isActive') isActive: boolean;

  constructor(private _store: Store) { }

  ngOnInit() {
  }

  changeSportEvent(){
    this._store.dispatch(new ChangeActiveSportEvent(this.sportEvent));
    if (this.sportEvent.event_state != 'PENDING') {
      this._store.dispatch(new GetScore(this.sportEvent));
    } else {
      this._store.dispatch(new RemoveScore);
    }
  }

}
