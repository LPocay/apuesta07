import { Component, OnInit, Input } from '@angular/core';
import { Bet } from '../../../models/sports';
import { Store } from '@ngxs/store';
import { RemoveBet } from '../../../actions/bet.actions';
import { scaleAnimation } from '../../../animations/scale.animation';

@Component({
  selector: 'app-bet-item',
  templateUrl: './bet-item.component.html',
  styleUrls: ['./bet-item.component.scss'],
  animations: [scaleAnimation]
})
export class BetItemComponent implements OnInit {

  @Input('bet') bet: Bet;
  @Input('isTicket') isTicket: boolean; // Esta Variable se usa para mostrar el icono de la x para poder reusar el component el la vista de tickets
  constructor(private _store: Store) { }

  ngOnInit() {
  }
  removeBet(bet: Bet) {
    this._store.dispatch(new RemoveBet(bet));
  }
}
