import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { ChangeStake } from '../../actions/bet.actions';
import { Bet } from '../../models/sports';
import { Observable } from 'rxjs';
import { BetService } from '../../services/bet.service';

@Component({
  selector: 'app-bet',
  templateUrl: './bet.component.html',
  styleUrls: ['./bet.component.scss']
})
export class BetComponent implements OnInit {

  @Select(state => state.bet.bets) bets$: Observable<Bet[]>;
  @Select(state => state.bet.total) total$: Observable<number>;
  stakeFormControl: FormControl; // Input de la apuesta
  total: number; // Total a pagar por la apuesta
  bets: Bet[]; // Apuestas agregadas

  constructor(private _store: Store, private _bet: BetService) { 
    this.stakeFormControl = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.stakeFormControl.valueChanges.subscribe(number => {
      this._store.dispatch(new ChangeStake(number));
    });
    this.total$.subscribe(total => this.total = total);
    this.bets$.subscribe(bets => this.bets = bets);
  }

  createBet() {
    if (this.bets.length && this.stakeFormControl.valid) {
      this._bet.createBet(this.bets, this.stakeFormControl.value).subscribe(
        response => {
          // Se informara si el se creo la apuesta o no
        }
      );
    }    
  }

}
