import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PointSpread, SportEvent, Bet, Team } from '../../../../models/sports';

@Component({
  selector: 'app-ps',
  templateUrl: './ps.component.html',
  styleUrls: ['./ps.component.scss']
})
export class PsComponent implements OnInit {

  @Input('ps') ps: PointSpread;
  @Input('sportEvent') sportEvent: SportEvent;
  @Output('onAddBet') onAddBet: EventEmitter<Bet> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  selectPointSpread(team: Team, money: string, spread: string) {
    let bet: Bet = {
      team_name: team.name,
      team_id: team.id,
      money: money,
      event_name: this.sportEvent.name,
      event_id: this.sportEvent.id,
      type: 'ps',
      spread: spread
    };
    this.onAddBet.emit(bet);
  }
}
