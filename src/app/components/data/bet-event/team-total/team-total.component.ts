import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TeamTotal, SportEvent, Bet, Team } from '../../../../models/sports';

@Component({
  selector: 'app-team-total',
  templateUrl: './team-total.component.html',
  styleUrls: ['./team-total.component.scss']
})
export class TeamTotalComponent implements OnInit {

  @Input('teamTotal') team_total: TeamTotal;
  @Input('sportEvent') sportEvent: SportEvent;
  @Output('onAddBet') onAddBet: EventEmitter<Bet> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  selectTeamTotal(team: Team, money: string, isUnder: boolean, total: string) {
    let bet: Bet = {
      team_name: team.name,
      team_id: team.id,
      money: money,
      event_name: this.sportEvent.name,
      event_id: this.sportEvent.id,
      isUnder: isUnder,
      type: 'team_total',
      total: total
    };
    this.onAddBet.emit(bet);
  }

}
