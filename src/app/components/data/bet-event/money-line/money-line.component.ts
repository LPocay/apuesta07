import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MoneyLine, SportEvent, Bet, Team } from '../../../../models/sports';

@Component({
  selector: 'app-money-line',
  templateUrl: './money-line.component.html',
  styleUrls: ['./money-line.component.scss']
})
export class MoneyLineComponent implements OnInit {

  @Input('moneyLine') moneyLine: MoneyLine;
  @Input('sportEvent') sportEvent: SportEvent;
  @Output('onAddBet') onAddBet: EventEmitter<Bet> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  selectMoneyLine(team: Team, money: string) {
    let bet: Bet = {
      team_name: team.name,
      team_id: team.id,
      money: money,
      event_name: this.sportEvent.name,
      event_id: this.sportEvent.id,
      type: 'money_line'
    };
    this.onAddBet.emit(bet);
  }
}
