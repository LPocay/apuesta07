import { Component, OnInit, Input } from '@angular/core';
import { Line, Bet, SportEvent, League } from '../../../models/sports';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { AddBet, ChangeBet } from '../../../actions/bet.actions';

@Component({
  selector: 'app-bet-event',
  templateUrl: './bet-event.component.html',
  styleUrls: ['./bet-event.component.scss']
})
export class BetEventComponent implements OnInit {

  @Input('lines') lines: Line[];
  @Select(state => state.schedule.activeSportEvent) activeSportEvent$: Observable<SportEvent>;
  @Select(state => state.schedule.activeLeague) activeLeague$: Observable<League>;
  @Select(state => state.bet.bets) bets$: Observable<Bet[]>;

  selectedBet: Bet;
  sportEvent: SportEvent;
  league: League;

  constructor(private _store: Store) { }

  ngOnInit() {
    this.activeSportEvent$.subscribe(sportEvent => this.sportEvent = sportEvent);
    this.bets$.subscribe(bets => {
      bets.forEach(bet => {
        if (bet.event_id === this.sportEvent.id) {
          this.selectedBet = bet;
        }
      });
      if(bets.indexOf(this.selectedBet) === -1){
        this.selectedBet = null;
      }
    });
  }

  selectBet(bet: Bet, line: Line) {
    bet.period_id = line.period_id;
    bet.sportbook = line.sportsbook;
    if(this.selectedBet) {
      if (this.selectedBet !== bet) {
        this._store.dispatch(new ChangeBet(bet));
      }
    } else {
      this._store.dispatch(new AddBet(bet));
      this.selectedBet = bet;
    }
  }

}
