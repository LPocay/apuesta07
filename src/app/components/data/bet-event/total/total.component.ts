import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Total, SportEvent, Bet, Team } from '../../../../models/sports';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.scss']
})
export class TotalComponent implements OnInit {

  @Input('total') total: Total;
  @Input('sportEvent') sportEvent: SportEvent;
  @Output('onAddBet') onAddBet: EventEmitter<Bet> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  selectTotal(money: string, isUnder: boolean) {
    let bet: Bet = {
      team_name: '',
      team_id: -1, // no se incluye ningun equipo por que este es el total de ambos equipos
      money: money,
      event_name: this.sportEvent.name,
      event_id: this.sportEvent.id,
      isUnder: isUnder,
      type: 'total',
      total: this.total.total
    };
    this.onAddBet.emit(bet);
  }

}
