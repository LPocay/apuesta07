import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ScoreEvent, SportEvent, EventLine } from '../../models/sports';
import { Select } from '@ngxs/store';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {
  
  @Select(state => state.score.scoreEvent) scoreEvent$: Observable<ScoreEvent>;
  @Select(state => state.schedule.activeSportEvent) sportEvent$: Observable<SportEvent>;
  @Select(state => state.line.event) eventLines$: Observable<EventLine[]>;
  @Select(state => state.line.isFetching) isFetching$: Observable<boolean>;

  scoreEvent: ScoreEvent;
  sportEvent: SportEvent;
  eventLines: EventLine[];
  isFetching: boolean; // Variable para saber si esta trayendo datos de la API

  constructor() { }

  ngOnInit() {
    this.isFetching$.subscribe(
      isf => {
        this.isFetching = isf;
      }
    );
  
    this.eventLines$.subscribe(
      lines => {
        console.log(lines);
        this.eventLines = lines;
      }
    );
  
    this.scoreEvent$.subscribe(
      scoreEvent => {
        this.scoreEvent = scoreEvent;
      }
    );

    this.sportEvent$.subscribe(
      sportEvent => {
        this.sportEvent = sportEvent;
      }
    );
  }

}
