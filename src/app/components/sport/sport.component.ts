import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Sport, League, Schedule } from '../../models/sports';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ChangeActiveSport, ChangeActiveLeague } from '../../actions/schedule.actions';

@Component({
  selector: 'app-sport',
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.scss']
})
export class SportComponent implements OnInit {
  // State of th App
  @Select(state => state.schedule.schedule) schedule$: Observable<Schedule>;
  @Select(state => state.schedule.activeSport) activeSport$: Observable<Sport>;
  @Select(state => state.schedule.activeLeague) activeLeague$: Observable<League>;

  // Local variables
  sports: Sport[] = [];
  sportsFiltered: Sport[];
  searchSportInput: FormControl;
  activeSport: Sport;
  activeLeague: League;
  isFetching: boolean = true;
  sportsIcons = {
    'Football': 'icon-football',
    'Baseball': 'icon-baseball',
    'Basketball': 'icon-basketball-1',
    'Soccer': 'icon-soccer',
    'Rugby': 'icon-rugby',
    'Golf': 'icon-golf',
    'Hockey': 'icon-hockey',
    'Boxing': 'icon-boxing',
    'Tennis': 'icon-tennis',
  };

  constructor(private _store: Store) { }

  ngOnInit() {
    this.schedule$.subscribe(
      schedule => {
        if (schedule) {
          if (this.sports.length){
            this.sports = schedule.sport;
            this.updateSchedule();
          } else {
            this.sports = schedule.sport;        
            this.sportsFiltered = this.sports;
            this.isFetching = false;
          }
        }
      }
    );

    this.activeSport$.subscribe(
      sport => {
        this.activeSport = sport;
      }
    );

    this.activeLeague$.subscribe(
      league => {
        this.activeLeague = league;
      }
    );
    this.searchSportInput = new FormControl();
    this.onSearchInputChange();
  }

  onSearchInputChange (): void {
    this.searchSportInput.valueChanges.pipe(debounceTime(500)).subscribe(
      value => {
        this.sportsFiltered = this.sports.filter(sport => {
          if (!value) {
            return true
          }
          if (sport.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())) {
            return true;
          } else {
            return false;
          }
        });
      }
    );
  }

  changeSport(sport: Sport): void {
    if (sport != this.activeSport) {
      if (!(sport.league instanceof Array)) {
        sport.league = [sport.league];
      }
      this._store.dispatch([new ChangeActiveSport(sport), new ChangeActiveLeague(sport.league[0])]);
    }
  }

  changeLeague(league: League): void {
    this._store.dispatch(new ChangeActiveLeague(league));
  }

  updateSchedule(): void {
    this.sports.map(sport => {
      if (this.activeSport.id === sport.id) {
        this._store.dispatch(new ChangeActiveSport(sport));
        sport.league.map(league => {
          if (this.activeLeague.id === league.id) {
            this._store.dispatch(new ChangeActiveLeague(league));
          }
        })
      }
    });
  }
}
