import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthService } from '../../services/auth.service';
import { GetSchedule } from '../../actions/schedule.actions';

@Component({
  selector: 'app-bet-columns',
  templateUrl: './bet-columns.component.html',
  styleUrls: ['./bet-columns.component.scss']
})
export class BetColumnsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, 
              private auth: AuthService,
              private _store: Store) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        let token = params['token'];
        this.auth.setToken(token);
        // una vez con el token se Inicia las consultas a la API
        this._store.dispatch(new GetSchedule());
      }
    );
  }

}
