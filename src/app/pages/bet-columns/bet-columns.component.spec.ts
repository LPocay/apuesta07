import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetColumnsComponent } from './bet-columns.component';

describe('BetColumnsComponent', () => {
  let component: BetColumnsComponent;
  let fixture: ComponentFixture<BetColumnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetColumnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
