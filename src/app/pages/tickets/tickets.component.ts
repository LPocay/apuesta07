import { Component, OnInit } from '@angular/core';
import { Ticket, Bet } from '../../models/sports';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {

  @Select(state => state.ticket.tickets) tickets$: Observable<Ticket[]>;
  tickets: Ticket[];
  filteredTickets: Ticket[];
  activeTicket: Ticket;
  bets: Bet[];

  startDate: Date;
  endDate: Date;

  searchIDInput: FormControl;
  constructor() { }

  ngOnInit() {
    this.tickets$.subscribe(
      tickets => {
        tickets = [
          {
            bets: [],
            date: new Date('10/11/2018').toDateString(),
            stake: 100,
            outcome: 4814,
            id: '1584'
          },
          {
            bets: [],
            date: new Date('10/5/2018').toDateString(),
            stake: 100,
            outcome: 4814,
            id: '1284'
          }
        ];
        this.tickets = tickets;
        this.filteredTickets = tickets;
        this.activeTicket = tickets[0];
        this.bets = [
          {
            event_id: 1,
            event_name: 'Evento 1',
            money: '200',
            team_id: 1,
            team_name: 'Equipo 1',
            type: 'money_line',
          },
          {
            event_id: 1,
            event_name: 'Evento 1',
            money: '200',
            team_id: 1,
            team_name: 'Equipo 1',
            type: 'money_line',
          },
          {
            event_id: 1,
            event_name: 'Evento 1',
            money: '200',
            team_id: 1,
            team_name: 'Equipo 1',
            type: 'money_line',
          },
          {
            event_id: 1,
            event_name: 'Evento 1',
            money: '200',
            team_id: 1,
            team_name: 'Equipo 1',
            type: 'money_line',
          },
          {
            event_id: 1,
            event_name: 'Evento 1',
            money: '200',
            team_id: 1,
            team_name: 'Equipo 1',
            type: 'money_line',
          },
        ];
      }
    );
    this.searchIDInput = new FormControl();
    this.onSearchInputChange();
  }

  onTicketClick(ticket: Ticket) {
    if(this.activeTicket.id !== ticket.id) {
      this.bets = ticket.bets;
      this.activeTicket = ticket;
    }
  }

  changeStartDate() {
    this.filteredTickets = this.tickets.filter(ticket => {
      if(new Date(ticket.date) > this.startDate){
        return true;
      } else {
        return false;
      }
    });
  }
  changeEndDate() {
    this.filteredTickets = this.tickets.filter(ticket => {
      if(new Date(ticket.date) < this.endDate){
        return true;
      } else {
        return false;
      }
    });
  }

  onSearchInputChange (): void {
    this.searchIDInput.valueChanges.pipe(debounceTime(500)).subscribe(
      value => {
        this.filteredTickets = this.tickets.filter(ticket => {
          if (!value) {
            return true
          }
          if (ticket.id.includes(value)) {
            return true;
          } else {
            return false;
          }
        });
      }
    );
  }

}
