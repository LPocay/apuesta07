import { trigger, transition, query, animate, stagger, style } from "@angular/animations";

export const elementListAnimation = trigger('elementListAnimation',[
    transition('* => *', [
        query('li', [
            style({ opacity: 0 })
        ], { optional: true }),
        query(':enter', [
            stagger(100, [
                style({ transform: 'translateY(10px)' }),
                animate('0.3s', style({opacity: 1, transform: 'translateY(0)'}))
            ])
        ], { optional: true })
    ])
]);