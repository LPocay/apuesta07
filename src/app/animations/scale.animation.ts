import { trigger, state, style, transition, animate, keyframes } from "@angular/animations";

export const scaleAnimation = trigger('scaleAnimation', [
    state('*', style({
        transform: 'scale(1)'
    })),
    transition('void => *', animate('0.25s', keyframes([
        style({
            transform: 'scale(0)'
        }),
        style({
            transform: 'scale(1.1)'
        }),
        style({
            transform: 'scale(0.9)'
        }),
        style({
            transform: 'scale(1)'
        }),
    ])))
]);