export interface Schedule {
    sport: Sport[];
}
export interface Sport {
    id: number;
    name: string;
    link: string;
    league: League[];
}

export interface League {
    id: number;
    name: string;
    link: string;
    group: Group[];
}

export interface Group {
    name: string;
    event: SportEvent[];
}

export interface SportEvent {
    id: number;
    name: string;
    season: string;
    date: string;
    event_type: string;
    event_state: string;
    time_changed: boolean;
    neutral: boolean;
    participant: Participant[];
}

export interface Participant {
    rot: string;
    side: string;
    team: Team;
}

export interface Team {
    id: number;
    name: string;
    link: string;
}

export interface Team {
    id: number;
    name: string;
}

export interface Score {
    rot: number;
    value: number;
}

export interface ScoreEvent {
    id: number;
    league_id: number;
    away_rot: number;
    home_rot: number;
    current_score: CurrentScore;
    period_summary: Period[]; 
}

export interface CurrentScore {
    away_score: number;
    home_score: number;
    description: string;
    time: string;
    period: string;
    period_id: number;
}

export interface Period {
    name: string;
    description: string;
    time: string;
    period_id: number;
    score: Score[];
}

export interface EventLine {
    id: number;
    date: string;
    line: Line[]
}

export interface Line {
    away_rot: number;
    home_rot: number;
    time: string;
    period_id: number;
    period: string;
    type: string;
    sportsbook: number;
    no_line?: boolean;
    ps?: PointSpread;
    money?: MoneyLine;
    total?: Total;
    team_total?: TeamTotal;
    display?: Display;
}

export interface PointSpread {
    away_spread: string;
    away_price: string;
    home_spread: string;
    home_price: string;
}

export interface MoneyLine {
    away_money: string;
    home_money: string;
    draw_money: string;
}

export interface Total {
    total: string
    over_price: string
    under_price: string
}

export interface TeamTotal {
    away_total: string;
    away_over_price: string;
    away_under_price: string;
    home_total: string;
    home_over_price: string;
    home_under_price: string;
}

export interface Display {
    away: string;
    home: string;
}

export interface Bet {
    id?: number; // el id de la apuesta (Cuando este se genere)
    money: string; // la cantidad de dinero que esta pagando la apuesta (en formato americano)
    type: 'money_line' | 'ps' | 'total' | 'team_total'; // el tipo de apuesta
    team_name: string; // Nombre del equipo
    team_id: number; // ID del equipo
    event_name: string; // nombre del evento
    event_id: number; // ID del evento
    period_id?: number; // ID del periodo
    sportbook?: number; // Libro de apuesta
    isUnder?: boolean; // Para saber si la apuesta es por debajo o por arriba (team_total y total)
    total?: string; // el total (solo aplica para apuesta de tipo total y team_total)
    spread?: string; // Cantidad de spread (solo para apuestas de tipo ps)
}

export interface Ticket {
    id?: string;
    bets: Bet[];
    stake: number;
    date?: string;
    outcome?: number;
}