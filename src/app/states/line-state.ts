import { EventLine } from "../models/sports";
import { State, Action, StateContext } from '@ngxs/store';
import { GetLineByLeague, GetLineByEvent, IsFetching } from "../actions/line.actions";
import { LineService } from "../services/line.service";
import { tap } from "rxjs/operators";

export interface LineStateModel {
    event: EventLine[];
    isFetching: boolean;
}

@State<LineStateModel>({
    name: 'line',
    defaults: {
        event: [],
        isFetching: false
    }
})
export class LineState {

    constructor(private _line: LineService){}

    @Action(GetLineByLeague)
    getLineByLeague (ctx: StateContext<LineStateModel>, action: GetLineByLeague) {
        return this._line.getLinesByLeague(action.league).pipe(tap((lines) => {
            const state = ctx.getState();
            ctx.setState({
                ...state,
                event: lines,
                isFetching: false
            })
        }));
    }

    @Action(GetLineByEvent)
    getLineByEvent (ctx: StateContext<LineStateModel>, action: GetLineByEvent) {
        return this._line.getLinesByEvent(action.league, action.sportEvent).pipe(tap((lines) => {
            const state = ctx.getState();
            let newLines: EventLine[];
            if (!(lines instanceof Array)) {
                newLines = [lines];
            }
            ctx.setState({
                ...state,
                event: newLines ? newLines : lines,
                isFetching: false
            })
        }));
    }

    @Action(IsFetching)
    isFetching (ctx: StateContext<LineStateModel>) {
        const state = ctx.getState();
            ctx.setState({
                ...state,
                isFetching: true
            })
    }
}