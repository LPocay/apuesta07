import { ScoreEvent } from "../models/sports";
import { State, Action, StateContext } from "@ngxs/store";
import { ScoreService } from "../services/score.service";
import { GetScore, RemoveScore } from "../actions/score.actions";
import { tap } from "rxjs/operators";

export interface ScoreStateModel {
    scoreEvent: ScoreEvent
}

@State<ScoreStateModel>({
    name: 'score',
    defaults: {
        scoreEvent: null
    }
})
export class ScoreState {
    constructor(private _score: ScoreService) {}

    @Action(GetScore)
    getScore (ctx: StateContext<ScoreStateModel>, action: GetScore) {
        return  this._score.getScore(action.sportEvent).pipe(tap((scoreEvent) => {
            const state = ctx.getState();
            ctx.setState({
                ...state,
                scoreEvent: scoreEvent
            });
        }));
    }

    @Action(RemoveScore)
    removeScore (ctx: StateContext<ScoreStateModel>) {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            scoreEvent: null
        });
    }
}