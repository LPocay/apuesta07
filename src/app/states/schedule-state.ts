import { State, Action, StateContext } from '@ngxs/store';
import { Sport, Schedule, League, Group, SportEvent } from '../models/sports';
import { GetSchedule, ChangeActiveSport, ChangeActiveLeague, ChangeActiveGroup, ChangeActiveSportEvent } from '../actions/schedule.actions';
import { ScheduleService } from '../services/schedule.service';
import { tap } from 'rxjs/operators';

export interface ScheduleStateModel {
    schedule: Schedule;
    activeSport: Sport;
    activeLeague: League;
    activeGroup: Group;
    activeSportEvent: SportEvent;
}

@State<ScheduleStateModel>({
    name: 'schedule',
    defaults: {
        schedule: null,
        activeSport: null,
        activeLeague: null,
        activeGroup: null,
        activeSportEvent: null
    }
})
export class ScheduleState {

    constructor(private _schedule: ScheduleService){}

    @Action(GetSchedule)
    getSchedule(ctx: StateContext<ScheduleStateModel>) {
        return this._schedule.getSchedule().pipe(tap((schedule) => {
            const state = ctx.getState();
            ctx.setState({
                ...state,
                schedule: schedule
            });
        })
        );
    }

    @Action(ChangeActiveSport)
    changeActiveSport(ctx: StateContext<ScheduleStateModel>, action: ChangeActiveSport) {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            activeSport: action.sport
        });
    }
    
    @Action(ChangeActiveLeague)
    changeActiveLeague(ctx: StateContext<ScheduleStateModel>, action: ChangeActiveLeague) {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            activeLeague: action.league
        });
    }

    @Action(ChangeActiveGroup)
    changeActiveGroup(ctx: StateContext<ScheduleStateModel>, action: ChangeActiveGroup) {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            activeGroup: action.group
        });
    }

    @Action(ChangeActiveSportEvent)
    changeActiveSportEvent(ctx: StateContext<ScheduleStateModel>, action: ChangeActiveSportEvent) {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            activeSportEvent: action.sportEvent
        });
    }
}
