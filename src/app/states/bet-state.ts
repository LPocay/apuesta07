import { State, Action, StateContext } from '@ngxs/store';
import { AddBet, ChangeStake, ChangeBet, RemoveBet } from '../actions/bet.actions';
import { Bet } from '../models/sports';

export interface BetStateModel {
    total: number;
    stake: number;
    bets: Bet[];
}

@State<BetStateModel>({
    name: 'bet',
    defaults: {
        total: 0,
        stake: 0,
        bets: []
    }
})
export class BetState {
    @Action(AddBet)
    addBet (ctx: StateContext<BetStateModel>, action: AddBet) {
        const state = ctx.getState();        
        let total = this.calcBet(state.bets, state.stake,action.bet);
        ctx.setState({
            ...state,
            bets: state.bets,
            total: total
        });
    }

    @Action(ChangeBet)
    changeBet (ctx: StateContext<BetStateModel>, action: ChangeBet) {
        const state = ctx.getState();
        let bets = state.bets.map(bet => {
            if(bet.event_id === action.bet.event_id) {
                return action.bet
            } else {
                return bet
            }
        });
        let total = this.calcBet(bets, state.stake);
        ctx.setState({
            ...state,
            total: total,
            bets: bets
        })
    }

    @Action(ChangeStake)
    changeStake(ctx: StateContext<BetStateModel>, action: ChangeStake) {
        const state = ctx.getState();
        let total = this.calcBet(state.bets, action.stake);
        ctx.setState({
            ...state,
            stake: action.stake,
            total: total
        });
    }

    @Action(RemoveBet)
    removeBet(ctx: StateContext<BetStateModel>, action: RemoveBet) {
        const state = ctx.getState();
        let bets = state.bets.filter((bet) => {
            return !(bet.event_id === action.bet.event_id)
        });
        let total = this.calcBet(bets, state.stake);
        ctx.setState({
            ...state,
            total: total,
            bets: bets
        });
    }

    private calcBet(bets: Bet[], stake: number, bet?: Bet): number {
        let total = 0;
        let payout = 1;
        if (bet) {            
            bets.push(bet);
        }
        bets.forEach(bet => {
            let decimal = this.convertToDecimal(bet.money);
            payout *= decimal;
        });
        total = Math.round((payout - 1) * stake);
        return total;
    }

    private convertToDecimal (oddStr: string): number {
        let result = 0;
        let odd = Number(oddStr);
        if (odd < 0) {
            result = (-100 / odd) + 1;
        } else {
            result = (odd / 100) + 1;
        }
        return result;
    }
}