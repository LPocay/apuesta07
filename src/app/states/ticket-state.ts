import { Ticket } from "../models/sports";
import { State, Action, StateContext } from "@ngxs/store";
import { tap } from "rxjs/operators";
import { GetTickets } from "../actions/ticket.actions";
import { TicketsService } from "../services/tickets.service";

export interface TicketStateModel {
    tickets: Ticket[]
}

@State<TicketStateModel>({
    name: 'ticket',
    defaults: {
        tickets: []
    }
})
export class TicketState {

    constructor(private _tickets: TicketsService) {}

    @Action(GetTickets)
    getTickets(ctx: StateContext<TicketStateModel>) {
        return this._tickets.getTickets().pipe(tap((tickets) => {
            const state = ctx.getState();
            ctx.setState({
                ...state,
                tickets: tickets
            });
        }))
    }
}