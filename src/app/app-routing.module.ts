import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BetColumnsComponent } from './pages/bet-columns/bet-columns.component';
import { TicketsComponent } from './pages/tickets/tickets.component';

const routes: Routes = [
  { path: 'bets' , component: BetColumnsComponent },
  { path: 'tickets', component: TicketsComponent },
  { path: '', redirectTo: '/bets', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
