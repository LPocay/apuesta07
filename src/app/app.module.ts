import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared-modules/material.module';
import { SportComponent } from './components/sport/sport.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataComponent } from './components/data/data.component';
import { BetComponent } from './components/bet/bet.component';
import { GamesComponent } from './components/games/games.component';
import { NgxsModule } from '@ngxs/store';
import { ScheduleState } from './states/schedule-state';
import { EventComponent } from './components/games/event/event.component';
import { ScoreState } from './states/score-state';
import { LineState } from './states/line-state';
import { LoaderComponent } from './components/loader/loader.component';
import { MoneyLineComponent } from './components/data/bet-event/money-line/money-line.component';
import { BetState } from './states/bet-state';
import { PsComponent } from './components/data/bet-event/ps/ps.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BetEventComponent } from './components/data/bet-event/bet-event.component';
import { TotalComponent } from './components/data/bet-event/total/total.component';
import { TeamTotalComponent } from './components/data/bet-event/team-total/team-total.component';
import { HoverscrollDirective } from './directives/hoverscroll.directive';
import { BetItemComponent } from './components/bet/bet-item/bet-item.component';
import { AppRoutingModule } from './/app-routing.module';
import { TicketsComponent } from './pages/tickets/tickets.component';
import { BetColumnsComponent } from './pages/bet-columns/bet-columns.component';
import { TokenInterceptor } from './services/token.interceptor';
import { TicketComponent } from './components/ticket/ticket.component';
import { TicketState } from './states/ticket-state';

@NgModule({
  declarations: [
    AppComponent,
    SportComponent,
    DataComponent,
    BetComponent,
    GamesComponent,
    EventComponent,
    LoaderComponent,
    MoneyLineComponent,
    PsComponent,
    BetEventComponent,
    TotalComponent,
    TeamTotalComponent,
    HoverscrollDirective,
    BetItemComponent,
    TicketsComponent,
    BetColumnsComponent,
    TicketComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    HttpClientModule,
    NgxsModule.forRoot([
      ScheduleState,
      ScoreState,
      LineState,
      BetState,
      TicketState
    ]),
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
