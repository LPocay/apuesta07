import { Bet } from "../models/sports";

export class AddBet {
    static readonly type = '[Bet] AddMoneyLine';
    constructor (public bet: Bet) {}
}

export class RemoveBet {
    static readonly type = '[Bet] RemoveBet';
    constructor(public bet: Bet) {}
}

export class ChangeBet {
    static readonly type = '[Bet] ChangeBet';
    constructor (public bet: Bet) {}
}

export class ChangeStake {
    static readonly type = '[Bet] ChangeStake'
    constructor(public stake: number) {}
}