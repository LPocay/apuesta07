import { League, SportEvent } from "../models/sports";

export class GetLineByLeague {
    static readonly type = '[Line] GetLine';
    constructor(public league: League){}
}

export class GetLineByEvent {
    static readonly type = '[Line] GetLineByID';
    constructor(public league: League, public sportEvent: SportEvent) {}
}

export class IsFetching {
    static readonly type = '[Line] IsFetching';
}