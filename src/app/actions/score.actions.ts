import { SportEvent } from "../models/sports";

export class GetScore {
    static readonly type = '[Score] GetScore';
    constructor(public sportEvent: SportEvent) {}
}

export class RemoveScore {
    static readonly type = '[Score] RemoveScore';
}