import { Sport, SportEvent, League, Group } from "../models/sports";

export class GetSchedule {
    static readonly type = '[Schedule] GetSport';
}

export class ChangeActiveSport {
    static readonly type = '[Schedule] ChangeActiveSport';
    constructor(public sport: Sport) {}
}

export class ChangeActiveLeague {
    static readonly type = '[Schedule] ChangeActiveLeague';
    constructor(public league: League) {}
}

export class ChangeActiveGroup {
    static readonly type = '[Schedule] ChangeActiveGroup';
    constructor(public group: Group) {}
}

export class ChangeActiveSportEvent {
    static readonly type = '[Schedule] ChangeActiveSportEvent';
    constructor(public sportEvent: SportEvent) {}
}