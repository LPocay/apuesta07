import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { League, SportEvent, EventLine } from '../models/sports';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LineService {

  constructor(private http: HttpClient) { }

  getLinesByLeague (league: League): Observable<EventLine[]> {
    return this.http.get<EventLine[]>(environment.BASE_URL + `/donbest/odds/${league.id}`).pipe(map((donbest: any) => donbest.don_best_sports.event));
  }

  getLinesByEvent (league: League, sportEvent: SportEvent): Observable<EventLine[]> {
    return this.http.get<EventLine[]>(environment.BASE_URL + `/donbest/odds/${league.id}/${sportEvent.id}`).pipe(map((donbest: any) => donbest.don_best_sports.event));
  }
}
