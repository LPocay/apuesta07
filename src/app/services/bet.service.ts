import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Bet, Ticket } from '../models/sports';

@Injectable({
  providedIn: 'root'
})
export class BetService {

  constructor(private http: HttpClient) { }

  createBet (bets: Bet[], stake: number):Observable<any> {
    let bet: Ticket = {
      bets: bets,
      stake: stake
    }
    return this.http.post<any>(environment.BASE_URL + '/bet/insertar', bet);
  }
}
