import { Injectable } from '@angular/core';
import { ScoreEvent, SportEvent } from '../models/sports';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {

  constructor(private http: HttpClient) { }

  getScore (sportEvent: SportEvent): Observable<ScoreEvent> {
    return this.http.get<ScoreEvent>(environment.BASE_URL + `/donbest/score/${sportEvent.id}`);
  }
}
