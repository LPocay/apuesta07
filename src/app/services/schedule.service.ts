import { Injectable } from '@angular/core';
import { Schedule } from '../models/sports';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) { }

  getSchedule(): Observable<Schedule> {
    return this.http.get<Schedule>(environment.BASE_URL + '/donbest/schedule').pipe(map((donbest: any) => donbest.don_best_sports.schedule));
  }
}
